input=readlines(ARGS[1])

check(rs, s) = foldl(&, occursin.(rs, s))

# *
rs1 = [r"(.*[aeiou]){3}", r"(.)\1"]
mapreduce(s->check(rs1, s)&&~occursin(r"ab|cd|pq|xy", s), +, input) |> println

# **
rs2 = [r"(.).\1", r"(..).*\1"]
mapreduce(s->check(rs2, s), +, input) |> println
