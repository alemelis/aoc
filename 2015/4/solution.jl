using MD5

input = readline(ARGS[1])

function solve(input, c=5, i=0)
    while (bytes2hex∘md5)(input*"$i")[1:c]≠"0"^c
        i+=1
    end
    i
end

# *
solve(input) |> println

# **
solve(input, 6) |> println
