input = readline(ARGS[1])
Δ = Dict('>'=>(1,0), '<'=>(-1,0), '^'=>(0,1), 'v'=>(0,-1))

function 🎅(input)
    visited = [(0,0)]
    for c in input
        push!(visited, visited[end].+Δ[c])
    end
    visited |> unique
end

# *
input |> 🎅 |> length |> println

# **
append!(🎅(input[1:2:end]), 🎅(input[2:2:end])) |> unique |> length |> println
