using LinearAlgebra
input = readlines(ARGS[1])
dims = input.|>l->split(l,"x")|>l->parse.(Int, l)

# *
sum(i->2(i⋅circshift(i,1))+prod(i)÷maximum(i),dims)|>println
# \cdot == i'*circshift(i,1)

# **
sum(i->minimum(2(i+circshift(i,1)))+prod(i),dims)|>println
