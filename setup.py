import os
from aocd.models import Puzzle

# https://github.com/wimglenn/advent-of-code-data
for year in range(2015, 2022):
    for day in range(1, 26):
        y, d = map(str, [year, day])
        os.makedirs(os.path.join(y, d), exist_ok=True)

        puzzle = Puzzle(year=year, day=day)
        with open(os.path.join(y, d, "input.txt"), 'w') as f:
            f.writelines(puzzle.input_data)
